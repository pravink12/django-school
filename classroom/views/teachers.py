from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.shortcuts import  redirect, render
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView
from ..forms import TeacherSignUpForm,classSchedule
from ..models import  User,classScheduling,attendTutorial
import datetime

class TeacherSignUpView(CreateView):
    model = User
    form_class = TeacherSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'teacher'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('teachers:classAdd')




@login_required
def addClass(request):  
    
    if request.user.is_authenticated:
        date= datetime.datetime.now() 
        classschedule = classScheduling.objects.all()
        attendtutorial = attendTutorial.objects.all()
        form=classSchedule()
        if(request.method=='POST'):
            form=classSchedule(request.POST)
            print(request.POST)
            print(form.is_valid())
            if(form.is_valid()):
                form.save(commit=True)
                return redirect('/')
        context={'form':form,'classschedule':classschedule,'attendtutorial':attendtutorial,'date':date}
        return render(request,'classroom/teachers/scheduleClass.html',context)
    else: 
        return redirect('home')


