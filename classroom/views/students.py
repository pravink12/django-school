from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.shortcuts import  redirect, render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, ListView, UpdateView

from ..forms import  StudentSignUpForm
from ..models import Student, User,classScheduling,attendTutorial
import datetime


class StudentSignUpView(CreateView):
    model = User
    form_class = StudentSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'student'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('students:attendClass')

@login_required
def attendClass(request):   
    date= datetime.datetime.now() 
    classscheduling = classScheduling.objects.all()
    context = {
        'classscheduling':classscheduling,
        'date':date,
    }        
    return render(request,'classroom/students/attendClass.html',context)


@login_required
def attendVideo(request):
    if request.method == "POST":
        if request.POST.get("startClass"):
            global classStart
            classStart = datetime.datetime.now()
            classStart = classStart.strftime("%m/%d/%Y, %H:%M:%S")
            record = attendTutorial(class_start=classStart)
            print('classStart',classStart)
            record.save()
        elif request.POST.get("exitClass"):
            classEnd = datetime.datetime.now()
            classEnd = classEnd.strftime("%m/%d/%Y, %H:%M:%S")
            record = attendTutorial(class_end=classEnd)
            activeTime = datetime.datetime.strptime(
                classEnd, "%m/%d/%Y, %H:%M:%S"
            ) - datetime.datetime.strptime(classStart, "%m/%d/%Y, %H:%M:%S")
            active = attendTutorial(active_time=activeTime)
            record.save()
            active.save()
            totalduration= classScheduling.objects.values('totalDuration')
            if activeTime< totalduration:
                Status = attendTutorial(status='Absent')
            else:
                Status = attendTutorial(status='Absent')            

    return redirect("students:attendClass")
    
