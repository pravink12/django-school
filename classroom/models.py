from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.html import escape, mark_safe


class User(AbstractUser):
    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)

class classScheduling(models.Model):
    classId = models.CharField(max_length=5)
    classScheduleDate = models.CharField(max_length=11)
    totalDuration = models.CharField(max_length=3)

class attendTutorial(models.Model):
    class_start= models.CharField(max_length=55)
    class_end= models.CharField(max_length=55)
    active_time =  models.CharField(max_length=55)
    status =  models.CharField(max_length=55, blank=True)
    
    

class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
   
    def __str__(self):
        return self.user.username
