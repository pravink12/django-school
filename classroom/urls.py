from django.urls import include, path

from .views import classroom, students, teachers

urlpatterns = [
    path('', classroom.home, name='home'),

    path('students/', include(([
        path('', students.attendClass, name='attendClass'),
        path('actionUrl', students.attendVideo,name='attendVideo'),

    ], 'classroom'), namespace='students')),

    path('teachers/', include(([
        path('', teachers.addClass, name='classAdd'),
    ], 'classroom'), namespace='teachers')),
]
